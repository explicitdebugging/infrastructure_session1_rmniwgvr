# [CardCount]

## Statement
You are dealing cards to a group of players.  In this game, the cards are numbered 0-9 and have no distinguishing 
characteristics other than their numbers.  A given deck may have many cards that share the same number.  You are given an  
int *numPlayers* that represents the number of people playing.  You are also given a String 
*deck* which contains the cards to be used (0th character is the top of the deck, and the last character is the bottom). 
 You will deal the cards starting with player 0, then player 1, until you reach player *numPlayers*-1, dealing one card 
at a time.  Then you repeat this process until the cards are exhausted.  Cards are always dealt from the top of the deck.  

You will return a String[] that shows each of the players' cards in the order they were dealt (cards dealt 
earlier show up earlier in the particular String).  Player k's hand corresponds to element k of 
the returned String[].  One added constraint is that each player must be dealt the same number of cards.  To  enforce this 
constraint you will not deal extra cards that will unbalance the players' hands (cards held).  In other words, if you have 
dealt to the last player, and don't have enough cards to deal another to every player, stop dealing.  The returned String[] must contain exactly *numPlayers* elements, each element containing exactly the same number of characters.

## Definitions
- *Class*: `CardCount`
- *Method*: `dealHands`
- *Parameters*: `int, String`
- *Returns*: `String[]`
- *Method signature*: `String[] dealHands(int numPlayers, String deck)`

## Constraints
- *numPlayers* will be between 1 and 50 inclusive
- *deck* will contain between 0 and 50 characters inclusive, all of which are numeric (0-9)

## Examples
### Example 1
#### Input
<c>6,<br />"012345012345012345"</c>
#### Output
<c>[ "000",  "111",  "222",  "333",  "444",  "555" ]</c>
#### Reason
Exactly enough to deal 3 to each player.

### Example 2
#### Input
<c>4,<br />"111122223333"</c>
#### Output
<c>[ "123",  "123",  "123",  "123" ]</c>
### Example 3
#### Input
<c>1,<br />"012345012345012345"</c>
#### Output
<c>[ "012345012345012345" ]</c>
#### Reason
Only a single player.

### Example 4
#### Input
<c>6,<br />"01234"</c>
#### Output
<c>[ "",  "",  "",  "",  "",  "" ]</c>
#### Reason
Not enough cards to deal 1 to each player.

### Example 5
#### Input
<c>2,<br />""</c>
#### Output
<c>[ "",  "" ]</c>
#### Reason
No cards.


