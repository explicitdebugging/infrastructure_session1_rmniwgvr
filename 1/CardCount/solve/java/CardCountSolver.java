import java.io.FileReader;
import java.io.FileWriter;

import java.util.List;
import java.util.ArrayList;

import org.topcoder.TopcoderReader;
import org.topcoder.TopcoderWriter;
import org.topcoder.TypeRef;

public class CardCountSolver {
    public static void main(String[] args) {
    try {
        TopcoderReader reader = new TopcoderReader(new FileReader(args[0]));
        int numPlayers = (Integer) reader.next(Integer.class);
        reader.next();
        
        String deck = (String) reader.next(String.class);
        reader.close();

        CardCount solver = new CardCount();
        TopcoderWriter writer = new TopcoderWriter(new FileWriter(args[1]));
        String[] result = solver.dealHands(numPlayers, deck);
        List<String> resultBoxed = new ArrayList<String>();
        for (int _i = 0; _i < result.length; ++_i) {
            resultBoxed.add(result[_i]);
        }
        writer.write(resultBoxed);
        writer.close();
    } catch (Exception err) {
        err.printStackTrace(System.err);
    }
    }
}
