import java.io.FileReader;
import java.io.FileWriter;

import java.util.List;
import java.util.ArrayList;

import org.topcoder.TopcoderReader;
import org.topcoder.TopcoderWriter;
import org.topcoder.TypeRef;

public class CountGameSolver {
    public static void main(String[] args) {
    try {
        TopcoderReader reader = new TopcoderReader(new FileReader(args[0]));
        int maxAdd = (Integer) reader.next(Integer.class);
        reader.next();
        
        int goal = (Integer) reader.next(Integer.class);
        reader.next();
        
        int next = (Integer) reader.next(Integer.class);
        reader.close();

        CountGame solver = new CountGame();
        TopcoderWriter writer = new TopcoderWriter(new FileWriter(args[1]));
        writer.write(solver.howMany(maxAdd, goal, next));
        writer.close();
    } catch (Exception err) {
        err.printStackTrace(System.err);
    }
    }
}
