public class CountGame{

	public int howMany(int maxAdd, int goal, int next){
		int temp=goal;
		//create an array of strategic numbers 
		int [] arr=new int[(int)Math.ceil(goal/(maxAdd+1))];
		for (int i=arr.length-1; i>=0 ; i--){
			arr[i]=temp;

			temp=temp-(maxAdd+1);

		}
		
		for (int i=0; i<arr.length; i++){

			if (next<=arr[i]){
				temp=arr[i]-next+1;
				if (temp<=maxAdd) return temp;
			}

		}

		return -1;



	}
}