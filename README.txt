*GENERAL*
=You have received a folder called "infrastructure_SESSION*_YOUR-ID". This contains one (numerically) labelled folder. It has a buggy code and its corresponding problem statement,test cases and README file.
==The README file contains a link to an on-line questionnaire that you should fill.
==Before attempting each question you should *first* go to this link to ensure it is working.
==You may also observe the content of the questionnaire. The content is the same for all sessions.
=You are required to correct the buggy codes in the folder and ensure that all its test cases are passing.
=For your convenience, You may copy/import the buggy code into your favorite IDE to help your debugging process.
